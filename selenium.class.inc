<?php

/**
 * @file
 * Entity contoller for the selenium server entity.
 */

class SeleniumServerBasicController extends DrupalDefaultEntityController {

  /**
   * Prepare the entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->sid = 0;
    $entity->title = '';
    $entity->type = 'selenium_server';
    $entity->bundle_type = 'server_bundle_type';
    $entity->uid = '';
    $entity->status = '';
    $entity->created = '';
    $entity->address = '';
    $entity->server = '';
    $entity->browser = '';
    $entity->port = '';
    return $entity;
  }

  /**
   * Saving/update the entity.
   */
  function save($entity) {
    $primary_keys = $entity->sid ? 'sid' : array();

    // Allow other modules to alther the entity.
    module_invoke_all('selenium_server_presave', 'selenium_server', $entity);

    // Create the entity.
    drupal_write_record('selenium', $entity, array());

    // Update/create the data in the field.
    if (empty($primary_keys)) {
      field_attach_insert('selenium_server', $entity);
      $invoking = 'create';
    }
    else {
      field_attach_update('selenium_server', $entity);
      $invoking = 'update';
    }

    // Let know other modules for the entity insert.
    module_invoke_all('selenium_server_' . $invoking, $entity);

    drupal_goto('selenium/' . $entity->sid);
  }

  /**
   * Delete the entity
   */
   function delete($entity) {
     // Let know of other modules for the delete.
     module_invoke_all('selenium_server_delete', $entity);

    // Delete.
     field_attach_delete('selenium_server', $entity);
     db_delete('selenium')
      ->condition('sid', $entity->sid, '=')
      ->execute();
   }
}
